#include <stdlib.h>
#include <stdio.h>

/*
 * El casting consiste en convertir un tipo de variable en otro, cuando esto sea posible y teniendo en cuenta que puede implicar perdidas de precision.
 * El casting a un tipo de variable se aplica con el operador (tipo) variable
 */
int main (void){
	// Convertir el numero decimal en d en un entero y almacenarlo en i
	float d = 2.5;
	int i = (int) d;
	
	printf("d: %f\ni: %d\n",d,i);

	return EXIT_SUCCESS;
}