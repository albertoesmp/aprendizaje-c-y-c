#include <stdlib.h>
#include <stdio.h>

/*
 * Utilizando casting imprimir el valor sin signo que corresponde al numero en la variable a
 */
int main (void){
	int a = -17;
	
	printf("a = %d\n(unsigned int) a = %u\n",a,(unsigned int) a);
	
	return EXIT_SUCCESS;
}