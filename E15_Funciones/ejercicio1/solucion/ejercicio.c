#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/*
 * Realizar un programa que pida 2 numeros decimales a un usuario y utilice funciones para calcular:
 *	- La raiz de la suma de los cuadrados (puede usarse la libreria matematica)
 *	- El menor de los dos numeros (si son iguales que devuelva uno cualquiera)
 *	- La suma del doble del primero mas el cuadrado del segundo
 */

void pedir_numeros(double * numero1, double * numero2);
double raiz_suma_cuadrados(double numero1, double numero2);
double obtener_menor(double numero1, double numero2);
double suma_doble_cuadrado(double numero1, double numero2);

// MAIN
int main (void){
	double numero1, numero2;
	pedir_numeros(&numero1,&numero2);
	
	printf("sqrt(%f^2 + %f^2) = %f\n",numero1,numero2,raiz_suma_cuadrados(numero1,numero2));
	printf("menor{%f , %f} = %f\n",numero1,numero2,obtener_menor(numero1,numero2));
	printf("2*%f+%f^2 = %f\n",numero1,numero2,suma_doble_cuadrado(numero1,numero2));
	
	return EXIT_SUCCESS;
}

// FUNCIONES
void pedir_numeros(double * numero1, double * numero2){
	printf("Introduce numero1: ");
	// Cuando se leen los double hay que indicar que es precision simple con %lf en vez de %f. Sino considerara que son floats. Al imprimir esto no influye pues c realiza automaticamente la conversion.
	scanf("%lf",numero1); 
	printf("Introduce numero2: ");
	scanf("%lf",numero2);
}

double raiz_suma_cuadrados(double numero1, double numero2){
	sqrt(numero1*numero1 + numero2*numero2);
}

double obtener_menor(double numero1, double numero2){
	if(numero1<numero2) return numero1;
	return numero2;
}

double suma_doble_cuadrado(double numero1, double numero2){
	return 2*numero1+numero2*numero2;
}
