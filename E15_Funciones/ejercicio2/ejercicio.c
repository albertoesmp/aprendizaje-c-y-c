#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 * Escribir una funcion que convierta letras en minuscula a mayuscula y comprobar
 * su funcionamiento con las cadenas en el array cadenas.
 * Imprimir resultados
 *
 * Es interesante la funcion strlen de la libreria string.h que da el numero de caracteres que tiene una cadena.
 *	El fin de una cadena viene delimitado por un caracter \0, por tanto al alojar una cadena es importante tener en cuenta
 *	que resulta necesario sitio para un ultimo caracter que indica fin de cadena.
 *	La funcion strlen no tiene en cuenta este caracter al devolver la longitud de la cadena.
 */
 
int main (void){
	char * cadenas[3] = {"Hola Mundo", "adios mundo", "jaJajAja"};

	return EXIT_SUCCESS;
}
