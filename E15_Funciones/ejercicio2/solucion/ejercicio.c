#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 * Escribir una funcion que convierta letras en minuscula a mayuscula y comprobar
 * su funcionamiento con las cadenas en el array cadenas.
 * Imprimir resultados
 *
 * Es interesante la funcion strlen de la libreria string.h que da el numero de caracteres que tiene una cadena.
 *	El fin de una cadena viene delimitado por un caracter \0, por tanto al alojar una cadena es importante tener en cuenta
 *	que resulta necesario sitio para un ultimo caracter que indica fin de cadena.
 *	La funcion strlen no tiene en cuenta este caracter al devolver la longitud de la cadena.
 */

char * convertir_a_mayusculas(char * cadena);
 
int main (void){
	char * cadenas[3] = {"Hola Mundo", "adios mundo", "jaJajAja"};
	
	char * cadenas_mayusculas[3];
	cadenas_mayusculas[0] = convertir_a_mayusculas(cadenas[0]);
	cadenas_mayusculas[1] = convertir_a_mayusculas(cadenas[1]);
	cadenas_mayusculas[2] = convertir_a_mayusculas(cadenas[2]);
	
	int i;
	for(i=0 ; i < 3 ; i++){
		printf("%s -----MAYUSCULAS-------> %s\n",cadenas[i],cadenas_mayusculas[i]);
		free(cadenas_mayusculas[i]); // Liberar las cadenas alojadas en el heap por convertir_a_mayusculas ahora que ya no se usaran mas
	}


	return EXIT_SUCCESS;
}

char * convertir_a_mayusculas(char * cadena){
	char * mayusculas = (char *) malloc(sizeof(strlen(cadena)+1));
	int i;
	for(i=0 ; i < strlen(cadena) ; i++){
		if(cadena[i]>='a' && cadena[i]<='z') mayusculas[i] = cadena[i]+'A'-'a';
		else mayusculas[i] = cadena[i];
	}
	mayusculas[i] = '\0';
	return mayusculas;
}