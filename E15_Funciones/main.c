#include <stdlib.h>
#include <stdio.h>

/*
 * Las funciones son un conjunto de instrucciones que llevan a caba alguna tarea.
 * Las funciones en c se declaran y se definen. Si no hay una declaracion previa a la definicion se considera
 * que la declaracion ocurre en la definicion.
 * Una funcion debe estar declarada antes de poder invocarla.
 *
 * La sintaxis de declaracion de funciones es:
 *	tipo_devolucion nombre_funcion(tipo nombre, tipo nombre...);
 * El tipo void significa nada o vacio.
 * Es obligatorio especificar la devolucion aunque sea void, esto no se aplica a los parametros.
 */
 

void hola_mundo(void); // Declaracion de la funcion hola_mundo que no devuelve nada ni recibe parametros

void contar_hasta(int n); // Declaracion de la funcion contar_hasta que recibe un parametro int n

void adios_mundo(void){ // Definicion con declaracion implicita de la funcion adios_mundo
	printf("HOLA MUNDO!\n");
}
 
int main (void){
	hola_mundo(); // Invocar a la funcion hola_mundo
	contar_hasta(5);
	adios_mundo(); // Invocar a la funcion adios_mundo

	return EXIT_SUCCESS;
}

void hola_mundo(void){
	printf("ADIOS MUNDO!\n");
}

void contar_hasta(int n){
	int i;
	for(i=0 ; i < n ; i++){
		printf("%d\n",i);
	}
}