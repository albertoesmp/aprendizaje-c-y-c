#include <stdlib.h>
#include <stdio.h>

/*
 * El bucle for tiene la siguiente sintaxis: for(inicializacion ; condicion ; operaciones post iteracion)
 *	incializacion: Operaciones que se ejecutan justo antes de comenzar a iterar sobre el bucle
 *	condicion: Condicion la cual mientras se cumpla se ejecutara el bucle
 *	operaciones post iteracion: Estas operaciones se ejecutan al concluir cada iteracion antes de la siguiente y antes de comprobar la condicion
 */
int main(void){
	// Ejemplo de programa que usa un bucle for para imprimir la tabla de multiplicar del 5
	int i;
	for(i=0 ; i <= 10 ; i++){
		printf("%2d x %2d = %d\n",5,i,5*i); // %2d significa imprimir un entero usando como minimo 2 huecos para numeros, de no ser necesarios dejara espacios
	}
}