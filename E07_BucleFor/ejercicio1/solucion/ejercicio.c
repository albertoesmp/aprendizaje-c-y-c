#include <stdlib.h>
#include <stdio.h>

/*
 * Escribir un programa que cuente e imprima la frase HOLA MUNDO 100 veces
 */
int main (void){
	int i;
	for(i=0 ; i < 100 ; i++){
		printf("%3d: HOLA MUNDO\n",(i+1));
	}

	return EXIT_SUCCESS;
}