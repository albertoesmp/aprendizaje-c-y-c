#include <stdlib.h> // Incluye la libreria estandar de C
#include <stdio.h> // Include la libreria de entrada/salida de C

/*
 * Declarar la funcion main:
 * 	La primera palabra "int" significa que la funcion devuelve un entero a su termino
 *	La segunda palabra "main" es el nombre de la funcion. El nombre main es especial y esta reservado para el punto de inicio de ejecucion del programa.
 *	La palabra void dentro de los parentesis indica que no se recibe ningun parametro.
 * El contenido entre las llaves {} es el cuerpo de la funcion, el conjunto de instrucciones a ejecutar.
 */
int main (void){
	printf("Hola mundo\n"); // Imprimir hola mundo
	
	return EXIT_SUCCESS; // Al finalizar la ejecucion indicar que finalizo con exito devolviendo el valor que corresponda
}
