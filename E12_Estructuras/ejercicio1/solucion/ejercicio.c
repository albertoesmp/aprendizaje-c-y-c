#include <stdlib.h>
#include <stdio.h>

/*
 * Crear una estructura usuario que permita almacenar nombre, password, email y edad.
 * Pedir al usuario que teclee los datos oportunos y almacenarlos usando la estructura creada, luego imprimirlos.
 */

struct s_usuario{
	char * nombre;
	char * password;
	char * email;
	int edad;
};
 
int main (void){
	struct s_usuario usuario;
	
	
	usuario.nombre = (char *) malloc(sizeof(char)*60);
	printf("Introduce tu nombre: ");
	scanf("%s",usuario.nombre);
	
	usuario.password = (char *) malloc(sizeof(char)*60);
	printf("Introduce tu password: ");
	scanf("%s",usuario.password);
	
	usuario.email = (char *) malloc(sizeof(char)*60);
	printf("Introduce tu email: ");
	scanf("%s",usuario.email);
	
	printf("Introduce tu edad: ");
	scanf("%d",&usuario.edad);
	
	printf("Estos son los datos que has introducido:\n");
	printf("\tNombre: %s\n\tPassword: %s\n\tEmail: %s\n\tEdad: %d\n",
		usuario.nombre, usuario.password, usuario.email, usuario.edad);

	return EXIT_SUCCESS;
}