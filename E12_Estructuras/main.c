#include <stdlib.h>
#include <stdio.h>

/*
 * Las estructuras en c son maneras de definir organizaciones de datos.
 * Puede obtenerse el tamano necesario para referenciar a una estructura con sizeof(struct nombre_estructura).
 * Se comportan como un tipo de datos en su definicion y para otras operaciones.
 */
 
// Definicion de la estructura usuario
struct s_usuario{
	char * nombre_usuario;
	char * password;
};
 
int main (void){
	// Uso de la estructura para registrar 3 usuarios y luego imprimir sus datos
	struct s_usuario usuario1 = {"Paco","abc123..."};
	struct s_usuario usuario2;
	usuario2.nombre_usuario = "Esperanza";
	usuario2.password = "hola1234";
	struct s_usuario * usuario3 = (struct s_usuario *) malloc(sizeof(struct s_usuario));
	(*usuario3).nombre_usuario = "Julian";
	usuario3->password = "qwerty69"; // El operador -> equivale a dereferenciar el puntero y acceder a un elemento, en este caso equivale a (*usuario3).password

	printf("usuario1:\n");
	printf("\tnombre_usuario: %s\n",usuario1.nombre_usuario); // \t escapa una tabulacion
	printf("\tpassword: %s\n\n",usuario1.password);
	
	printf("usuario2:\n");
	printf("\tnombre_usuario: %s\n",usuario2.nombre_usuario);
	printf("\tpassword: %s\n\n",usuario2.password);
	
	printf("usuario3:\n");
	printf("\tnombre_usuario: %s\n",(*usuario3).nombre_usuario);
	printf("\tpassword: %s\n",usuario3->password);
	
	free(usuario3);
	
	return EXIT_SUCCESS;
}