#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 * Hacer un programa que si recibe como argumento de lanzamiento un numero 1 responda diciendo: "Eres el numero 1!"
 * Si no recibe un 1 que diga cualquier otra cosa.+
 *
 * Puede ser interesante usar la funcion strcmp que permite comparar cadenas tal que strmp(cadena1,cadena2)
 *	Devuelve un numero negativo si el primer byte que no coincide es menor en cadena1 que en cadena2
 *	Devuelve 0 si las cadenas eran iguales
 *	Devuelve un numero positivo si el primer byte que no coincide es mayor en cadena1 que en cadena2
 */
 
int main (int argc, char ** argv){
	int i;
	int numeroUno = 0;
	for(i=1 ; i < argc ; i++){
		if(strcmp("1",argv[i])==0){
			numeroUno=1;
			break;
		}
	}

	if(numeroUno)
		printf("Eres el numero 1!\n");
	else
		printf("No tengo nada que decirte.\n");
	
	return EXIT_SUCCESS;
}