#include <stdlib.h>
#include <stdio.h>

/*
 * Hasta ahora se ha usado el metodo main sin argumentos, void.
 * No obstante se pueden especificar argumentos de lanzamientos al invocar al comando.
 * Para recibir estos se definen los parametros int argc y char ** argv en la funcion main.
 *
 * int argc -> Numero de argumentos recibidos
 * char ** argv -> Array de cadenas, cada cadena es una palabra de argumento. Los espacios actuan como separadores de argumentos de lanzamiento.
 *	IMPORTANTE: El primer valor de argv[0] corresponde a la instruccion que lanza al comando
 */
int main (int argc, char ** argv){
	// Programa que imprime los argumentos recibidos (cuando se ejecute probar a indicar argumentos)
	int i;
	
	for(i=1 ; i < argc ; i++){
		printf("El comando \"%s\" ha recibido como argumento numero %d: %s\n",argv[0],i,argv[i]);
	}
}