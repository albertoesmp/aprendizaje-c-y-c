#include <stdlib.h>
#include <stdio.h>

/*
 * Crea un bucle de funcionamiento que simule a una terminal, consola o shell.
 * El bucle consistira en imprimir las opciones de un menu, permitir al usuario elegir una.
 * La eleccion del usuario se controla con un switch mediante el cual se ejecuta la orden correspondiente.
 * Las funciones a las que debe dar soporte el menu son:
 *	- Introducir Nombre (Permite al usuario introducir un nombre por teclado)
 *	- Saludar (Saluda al usuario conociendo su nombre, si no conoce su nombre muestra un mensaje distinto)
 *	- Despedir (Despide al usuario conociendo su nombre, si no conoco su nombre muestra un mensaje distinto)
 *	- Salir (Finaliza el programa)
 */

typedef enum enum_opciones{
	INTRODUCIR_NOMBRE,
	SALUDAR,
	DESPEDIR,
	SALIR
}Opcion;
 
void print_lineas_vacias(void);
void print_menu(void);
Opcion pedir_opcion(void);
char * pedir_nombre(void);
void saludar(char * nombre);
void despedir(char * nombre);
 
int main (void){
	Opcion opcion;
	char * nombre = NULL;
	do{
		print_lineas_vacias();
		print_menu();
		opcion = pedir_opcion();
		switch(opcion){
			case INTRODUCIR_NOMBRE:
				if(nombre!=NULL) free(nombre);
				nombre = pedir_nombre();
				break;
			case SALUDAR:
				saludar(nombre);
				break;
			case DESPEDIR:
				despedir(nombre);
				break;
			case SALIR:
				printf("Fin de la ejecucion!\n");
				break;
		}
	}while(opcion!=SALIR);

	if(nombre!=NULL) free(nombre);
	
	return EXIT_SUCCESS;
}

void print_lineas_vacias(void){
	int i;
	for(i=0 ; i < 5 ; i++) printf("\n");
}

void print_menu(void){
	printf("==========  M E N U  ==========\n");
	printf("%d. INTRODUCIR NOMBRE\n",INTRODUCIR_NOMBRE);
	printf("%d. SALUDAR\n",SALUDAR);
	printf("%d. DESPEDIR\n",DESPEDIR);
	printf("%d. SALIR\n\n",SALIR);
}

Opcion pedir_opcion(void){
	printf("Elige opcion: ");
	Opcion op;
	scanf("%d",&op);
	return op;
}

char * pedir_nombre(void){
	char * nombre = (char *) malloc(sizeof(char)*60);
	printf("Introduce nombre: ");
	scanf("%s",nombre);
	return nombre;
}

void saludar(char * nombre){
	if(nombre!=NULL) printf("Hola %s!\n",nombre);
	else printf("No saludo a desconocidos.\n");
}

void despedir(char * nombre){
	if(nombre!=NULL) printf("Adios %s!\n",nombre);
	else printf("Como quieres que te despida si no se quien eres?\n");
}
