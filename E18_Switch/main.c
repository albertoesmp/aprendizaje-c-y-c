#include <stdlib.h>
#include <stdio.h>

/*
 * La instruccion switch permite seleccionar una variable y especificar como actuar en caso de que su valor coincida con el asociado al caso.
 * Es especialmente descriptivo usar la instruccion switch con enumeraciones. Aqui se hara asi.
 *
 * La sintaxis de switch es como sigue:
 *	switch(variable){
 *		case valor1:
 *			instruccionesA;
 *			break;
 *		case valor2:
		case valor3:
			instruccionesB;
		case valor4:
			instruccionesC;
			break;
		default:
			instruccionesD;
			break;
	};
 * La palabra break indica que se rompe el switch, si no se indica, como es el caso de valor2 y valor3, tras la ejecucion de instruccionesB
 *	se introducira el bloque de instrucciones C.
 * Las instrucciones tras un case se ejecutar cuando el valor de la variable coincida con el que sigue tras el case.
 *	El bloque default es el que se ejecutara por defecto cuando no haya coincidencia con los valores de otros casos.
 */
 
enum colores{ rojo, verde, azul};

int main (void){
	// Programa igual al E17_Enumeraciones que utiliza un switch en lugar de ifs
	printf("COLORES\n");
	printf("%d) rojo\n%d) verde\n%d) azul\n",rojo, verde, azul);
	printf("\nElige un color indicando el numero correspondiente: ");
	enum colores color;
	scanf("%d",&color);
	
	char * nombre_color;
	
	switch(color){
		default:
			nombre_color = "ERROR";
			break; // La instruccion break indica que se rompe el switch, sino se ejecutarian los siguientes casos aunque no se cumpla la condicion necesaria
		case rojo:
			nombre_color = "rojo";
			break;
		case verde:
			nombre_color = "verde";
			break;
		case azul:
			nombre_color = "azul";
			break;
	}
	
	printf("Has elegido el color: %s\n",nombre_color);
	
	return EXIT_SUCCESS;
}