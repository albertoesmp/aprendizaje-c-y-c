#include <stdlib.h>
#include <stdio.h>

/*
 * Definir una enumeracion con el nombre unidades_temporales que contemple los elementos segundo, minuto y hora.
 * Si se ha hecho correctamente se podra compilar y ejecutar este programa con exito.
 */

enum unidades_temporales{
	segundo,
	minuto,
	hora
};

long obtener_segundos(enum unidades_temporales unidad_medida, long medida);

int main (void){
	enum unidades_temporales unidad_medida1 = segundo;
	enum unidades_temporales unidad_medida2 = hora;
	enum unidades_temporales unidad_medida3 = minuto;
	
	long medida1 = 41;
	long medida2 = 3;
	long medida3 = 17;
	
	long total_segundos = 0;
	total_segundos += obtener_segundos(unidad_medida1, medida1);
	total_segundos += obtener_segundos(unidad_medida2, medida2);
	total_segundos += obtener_segundos(unidad_medida3, medida3);
	
	printf("total de segundos: %ld\n",total_segundos);

	return EXIT_SUCCESS;
}

long obtener_segundos(enum unidades_temporales unidad_medida, long medida){
	long segundos;
	
	switch(unidad_medida){
		case segundo:
			segundos = medida;
			break;
		case minuto:
			segundos = medida*60;
			break;
		case hora:
			segundos = medida*60*60;
			break;
	}
	
	return segundos;
}
