#include <stdlib.h>
#include <stdio.h>

/*
 * Las enumeraciones son un tipo de datos que asocian valores numericos con nombres.
 * Sirven para hacer mas legible el codigo y ademas se llevan especialmente bien con la
 * instruccion switch que se vera mas adelante.
 */
 
enum colores{ rojo, verde, azul};

int main (void){
	// Programa que pide al usuario que seleccione un color
	printf("COLORES\n");
	printf("%d) rojo\n%d) verde\n%d) azul\n",rojo, verde, azul);
	printf("\nElige un color indicando el numero correspondiente: ");
	int color;
	scanf("%d",&color);
	
	char * nombre_color = "ERROR";
	if(color==rojo) nombre_color = "rojo";
	else if(color==verde) nombre_color = "verde";
	else if(color==azul) nombre_color = "azul";
	
	printf("Has elegido el color: %s\n",nombre_color);
	
	return EXIT_SUCCESS;
}