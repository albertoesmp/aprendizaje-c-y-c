#include <stdlib.h>
#include <stdio.h>

/*
 * La funcion scanf es especialmente util para leer de consola lo que escriba por teclado el usuario.
 * Existen otras funciones y mecanismo para obtener entrada de datos del usuario, por ahora se comenta esta.
 *
 * La sintaxis de scanf es similar a la de printf pero las variables en lugar de ser de salida son de entrada.
 *	Ej: scanf(%d,variable); Leer un entero y lo almacena en la variable, la cual debe ser un puntero.
 *		En c cuando una funcion modifica datos debe recibir un puntero al dato. Esto se debe a que las variables
 *		se pasan por valor por tanto, para modificar el valor de una variable, debe pasarse el valor del puntero para
 *		poder dereferenciarlo y manipular la direccion de memoria a la que apunta.
 */
int main (void){
	// Se pregunta al usuario su edad y se comprueba si es mayor de edad
	int edad;
	printf("Introduce tu edad: ");
	scanf("%d",&edad);
	
	if(edad>=18) printf("Eres mayor de edad.\n");
	else printf("NO eres mayor de edad.\n");

	return EXIT_SUCCESS;
}