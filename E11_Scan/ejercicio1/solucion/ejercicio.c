#include <stdlib.h>
#include <stdio.h>

/*
 * Hacer un juego de adivinar el numero.
 * El usuario tiene 10 intentos para adivinar un numero del 1 al 100.
 * Por cada respuesta que de se le dira si el numero que tiene que adivinar es mayor o menor que el que ha introducido.
 * Si el numero coincide se acaba el juego
 */

int main (void){
	int numero = 63, intento, intentos;

	for(intentos = 0 ; intentos < 10 ; intentos++){
		printf("Intenta adivinar el numero: ");
		scanf("%d",&intento);
		if(intento==numero){
			printf("Enhorabuena, has acertado el numero (%d)!",numero);
			break; // Con este break se sale del bucle for
		}
		else if(numero<intento){
			printf("El numero que tienes que adivinar es MENOR que el que has dicho (%d).\n",intento);
		}
		else{
			printf("El numero que tienes que adivinar es MAYOR que el que has dicho (%d).\n",intento);
		}
	}
	
	if(intentos==10) printf("Has perdido!\n");

	return EXIT_SUCCESS;
}