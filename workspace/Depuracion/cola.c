#include "cola.h"

// *** TIPOS DE DATOS *** //
// ********************** //
#define __TBE 3 // Tamano del bloque de elementos

typedef struct s_elemento_cola{
	Elemento * elementos; // Este elemento
	int numero_elementos; // numero de elementos en elementos
	struct s_elemento_cola * siguiente_elemento; // Elemento siguiente a este en la cola
} ElementoCola;

struct s_cola{ // Estructura utilizada para almacenar los elementos de la cola
	ElementoCola * elemento;
	
}; 

// DECLARACIONES
ElementoCola * __crear_elemento_cola(Elemento elemento);
ElementoCola * __obtener_ultimo_elemento_cola(struct s_cola c);
int __is_lleno(ElementoCola * ec);
void __insertar_elemento_en_elemento_cola(Elemento e, ElementoCola * ec);
Elemento __obtener_siguiente(struct s_cola cola);
void __reorganizar(struct s_cola * cola);

// *** FUNCIONES *** //
// ***************** //
void crear (Cola * cola){
	*cola = (struct s_cola *) malloc(sizeof(struct s_cola));
	struct s_cola * c = (struct s_cola *) *cola;
	
	c->elemento = NULL;
}

void destruir(Cola * cola){
	struct s_cola * c = (struct s_cola *) *cola;
	
	// Vaciar cola
	if(c->elemento!=NULL){
		ElementoCola * ec = NULL;
		do{
			ec = c->elemento;
			if(ec->siguiente_elemento!=NULL) c->elemento=ec->siguiente_elemento;
			free(ec->elementos);
			free(ec);
		
		}while(c->elemento!=NULL);
	}
	
	// Liberar cola
	free(c);
}

void insertar (Cola * cola, Elemento elemento){
	struct s_cola * c = (struct s_cola *) *cola;
	
	if(c->elemento==NULL) c->elemento = __crear_elemento_cola(elemento);
	else{
		ElementoCola * ultimo = __obtener_ultimo_elemento_cola(*c);
		if(__is_lleno(ultimo)) ultimo->siguiente_elemento = __crear_elemento_cola(elemento);
		else __insertar_elemento_en_elemento_cola(elemento,ultimo);
	}
}

int tiene_siguiente(Cola cola){
	struct s_cola * c = (struct s_cola *) cola;
	return c->elemento!=NULL;

}

Elemento siguiente (Cola * cola){
	struct s_cola * c = (struct s_cola *) *cola;
	Elemento e = __obtener_siguiente(*c);
	__reorganizar(c);
	return e;
}

// *** FUNCIONES INTERNAS *** //
// ************************** //
/*
 * Crea un ElementoCola en el heap.
 * Dicho elemento cola contendra como unico elemento el recibido como parametro
 */
ElementoCola * __crear_elemento_cola(Elemento elemento){
	ElementoCola * ec = (ElementoCola *) malloc(sizeof(ElementoCola));
	ec->elementos = (Elemento *) malloc(sizeof(Elemento));
	ec->elementos[0] = elemento;
	ec->numero_elementos = 1;
	return ec;
}

/*
 * Obtiene el ultimo ElementoCola alcanzable desde la cola recibida como parametro
 */
ElementoCola * __obtener_ultimo_elemento_cola(struct s_cola c){
	ElementoCola * ec = c.elemento;
	ElementoCola * aux = ec;
	while((aux=ec->siguiente_elemento)!=NULL) ec=aux;
	return ec;
}

int __is_lleno(ElementoCola * ec){
	return ec->numero_elementos == __TBE;
}

/*
 * Inserta el elemento en el ElementoCola, el cual debe existir y no estar lleno
 */
void __insertar_elemento_en_elemento_cola(Elemento e, ElementoCola * ec){
	ec->numero_elementos++;
	ec->elementos = (Elemento *) realloc(ec->elementos, sizeof(Elemento)*ec->numero_elementos);
	ec->elementos[ec->numero_elementos-1] = e;
}

/*
 * Obtiene el siguiente elemento de la cola al que le toca salir
 */
Elemento __obtener_siguiente(struct s_cola cola){
	return cola.elemento->elementos[0];
}

void __reorganizar(struct s_cola * cola){
	// Desplazar los elementos para ocupar el hueco del que acaba de salir
	int i;
	for(i=0 ; i < cola->elemento->numero_elementos-1 ; i++){
		cola->elemento->elementos[i] = cola->elemento->elementos[i+1];
	}
	
	// Mientras haya siguiente_elemento desplazar sus elementos una posicion mas proximos a la salida de la cola
	ElementoCola * ec_anterior = cola->elemento;
	ElementoCola * ec;
	ElementoCola * ec_anterior_al_anterior; // Para poder desvincular el anterior si este queda sin elementos
	if(ec_anterior->siguiente_elemento==NULL){
		ec_anterior->numero_elementos--;
		if(ec_anterior->numero_elementos>0){
			ec_anterior->elementos = (Elemento *) realloc(ec_anterior->elementos,sizeof(Elemento)*ec_anterior->numero_elementos);
		}
		else{
			free(ec_anterior->elementos);
			ec_anterior->elementos=NULL;
			cola->elemento=NULL;
		}
	}
	else{
		while( (ec=ec_anterior->siguiente_elemento) != NULL){
			ec_anterior->elementos[__TBE-1] = ec->elementos[0];
			for(i=0 ; i < ec->numero_elementos-1 ; i++){
				ec->elementos[i] = ec->elementos[i+1];
			}
			
			ec_anterior_al_anterior = ec_anterior;
			ec_anterior = ec;
		}
	
		// A partir de aqui ec_anterior es el ultimo ElementoCola con elementos
		ec_anterior->numero_elementos--;
		if(ec_anterior->numero_elementos>0)
			ec_anterior->elementos = (Elemento *) realloc(ec_anterior->elementos, sizeof(Elemento)*ec_anterior->numero_elementos);
		else{
			free(ec_anterior->elementos);
			ec_anterior->elementos=NULL;
			ec_anterior_al_anterior->siguiente_elemento = NULL;
			free(ec_anterior);
		}
	
	}
	
}