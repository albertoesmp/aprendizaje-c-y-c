#include <stdlib.h>
#include <stdio.h>

/*
 * Existe una variante del bucle while a la que se conoce como do...while.
 * En esta version del bucle while se determina un conjunto de instrucciones en el ambito del do, tal que: do{instrucciones}
 * Nada mas acabar ese ambito se coloca un while(condicion);
 *
 * El bloque de instrucciones del do se ejecutara minimo una vez, la primera y despues seguira ejecutandose mientras se cumpla
 * la condicion del while.
 */
int main (void){
	// Imprimir el valor de a en un bucle cuya condicion sea que a sea par y vaya aumentando el valor de a en cada iteracion de 1 en 1.
	int a = 1;
	do{
		printf("a: %d\n",a);
		a++;
	}while(a%2==0); // % es el operador modulo. a % 2 devuelve el resto de la division a / 2

	return EXIT_SUCCESS;
}