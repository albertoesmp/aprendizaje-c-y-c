#include <stdlib.h>
#include <stdio.h>

/*
 * Definir el tipo de dato natural que permita representar unicamente numeros naturales (considerando el 0 como numero natural).
 * Probar a realizar una operacion de suma, una de resta y una de multiplicacion utilizando los numeros naturales e imprimirlas.
 */

typedef unsigned long natural;
int main (void){
	natural a = 3;
	natural b = 2;
	
	printf("%lu + %lu = %lu\n",a,b,a+b);
	printf("%lu - %lu = %lu\n",a,b,a-b);
	printf("%lu * %lu = %lu\n",a,b,a*b);

	return EXIT_SUCCESS;
}