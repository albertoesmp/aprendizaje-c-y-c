#include <stdlib.h>
#include <stdio.h>

/*
 * Con la palabra reservada typedef pueden definirse tipos propios con una especie de alias.
 */
 
typedef char * Nombre; // Define el tipo Nombre que es un char *
typedef float Estatura; // Define el tipo Estatura que es un float

typedef struct{ // Define el tipo Persona que es una struct
	Nombre nombre;
	Estatura estatura;
} Persona;

int main (void){
	Persona persona;
	persona.nombre = "Mario";
	persona.estatura = 1.73;
	
	printf("%s mide %f\n",persona.nombre,persona.estatura);

	return EXIT_SUCCESS;
}