/*
 * Hacer que el programa se ejecute mientras alguna de las variables a, b, c sea menor que 100.
 * Al salir del bucle mostrar el resultado de la suma de las 3 variables.
 */
 
#include <stdlib.h>
#include <stdio.h>
 
int main (void){
	int a = 5;
	int b = 17;
	int c = 32;
	
	while( a<100 || b<100 || c<100){
		a++;
		b+=2;
		c+=3;
	}
	
	printf("%d+%d+%d = %d\n",a,b,c,a+b+c);
	
	return EXIT_SUCCESS;
}