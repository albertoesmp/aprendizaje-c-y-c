#include <stdlib.h>
#include <stdio.h>

int main(void){
	int numero = 0;

	/*
	 * Los bucles while se ejecutan mientras se cumpla la condicion correspondiente
	 *
	 * En este caso mientras el valor de la variable numero sea menor que 10 el conjunto de instrucciones
	 * del ambito del bucle while (entre llaves {}) se ejecutara
	 */
	while(numero<10){
		numero++; // El operador ++ significa incremento unitario sobre la variable, es decir aumentar su valor en 1. Equivale a numero+=1
	}
	
	printf("Numero: %d\n",numero);
	
	return EXIT_SUCCESS;
}