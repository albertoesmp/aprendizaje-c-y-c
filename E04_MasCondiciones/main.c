#include <stdlib.h>
#include <stdio.h>

/**
 * Existen otro tipo de condiciones mas alla de las comparaciones de mayor, menor, igual o diferente.
 * El AND y el OR
 *	AND: a && b -> Significa que deben ser ciertos los terminos a y b para que la evaluacion del AND (&&) sea cierta. Sino sera falsa.
 *	OR: a || b -> Significa que debe ser cierto uno de los terminos a o b. Con que uno sea cierto la condicion sera cierta, si los dos son falsos sera falsa.
 */
int main (void){
	int cierto = 1; // 1-> Cierto
	int falso = 0; // 0-> Falso
	
	// Operaciones AND
	if(cierto && cierto){
		printf("cierto AND cierto es CIERTO\n");
	}
	else{
		printf("cierto and cierto es FALSO\n");
	}
	
	if(cierto && falso){
		printf("cierto AND falso es CIERTO\n");
	}
	else{
		printf("cierto AND falso es FALSO\n");
	}
	
	if(falso && falso){
		printf("falso AND falso es CIERTO\n");
	}
	else{
		printf("falso AND falso es FALSO\n");
	}
	
	// Operaciones OR
	if(cierto || cierto){
		printf("cierto OR cierto es CIERTO\n");
	}
	else{
		printf("cierto OR cierto es FALSO\n");
	}
	
	if(cierto || falso){
		printf("cierto OR falso es CIERTO\n");
	}
	else{
		printf("cierto OR falso es FALSO\n");
	}
	
	if(falso || falso){
		printf("falso OR falso es CIERTO\n");
	}
	else{
		printf("falso OR falso es FALSO\n");
	}
	
}