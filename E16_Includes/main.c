#include <stdlib.h>
#include <stdio.h>

/*
 * Include, en c, es una directiva de precompilacion que indica al compilador que debe importar la cabecera o archivo de codigo correspondiente.
 *	#include "ruta/archivo.h" Incluye un archivo de cabecera dando la ruta desde el directorio local o bien una ruta absoluta
 *	#include <libreria.h> Incluye el archivo libreria.h el cual debe estar estar en directorios indicados previamente al compilador
 */
 
// Incluye el archivo misfunciones.h que solo declara funciones, sus definiciones estan en misfunciones.c el cual se debe mandar al compilador
#include "misfunciones.h"


/*
 * Ejemplo para compilar este programa con gcc:  gcc main.c misfunciones.c -o main -Wall -Wextra
 *	main.c y misfunciones.c son los archivos que parsea el compilador
 *	-o main indica que el archivo de salida generado sera main
 *	-Wall indica al compilador que muestre todos los warnings
 *	-Wextra indica al compilador que muestre warnings extra
 *	NOTA: Los warnings son avisos en compilacion que no son errores pero pueden significar que algo es susceptible de estar mal o ser mejorable
 */

int main (void){
	int a = 2, b = 3;
	int suma = sumar(a,b); // la funcion sumar esta declarada en misfunciones.h y definida en misfunciones.c 
	printf("%d + %d = %d\n",a,b,suma);

	return EXIT_SUCCESS;
}
