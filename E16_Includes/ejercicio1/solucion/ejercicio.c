#include <stdlib.h>
#include <stdio.h>
#include "aritmetica.h"

/*
 * Crear una libreria: aritmetica.h
 * Esta libreria debe definir las siguientes funciones:
 *	int sumar(int a, int b);
 *	int restar(int a, int b);
 *	int multiplicar(int a, int b);
 * Luego incluir dicha libreria en este programa. Si se ha hecho bien deberia poder compilarse y ejecutarse con exito.
 *
 * Para compilar con gcc puede utilizarse la siguiente instruccion: gcc ejercicio.c aritmetica.c -o ejercicio.exe -Wall -Wextra
 */
 
int main (void){
	int a = 3;
	int b = 2;
	
	printf("%d + %d = %d\n",a,b,sumar(a,b));
	printf("%d - %d = %d\n",a,b,restar(a,b));
	printf("%d * %d = %d\n",a,b,multiplicar(a,b));
	
	return EXIT_SUCCESS;
}