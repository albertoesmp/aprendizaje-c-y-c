#include <stdlib.h>
#include <stdio.h>

/*
 * Los punteros son direcciones de memoria.
 * En general existen 2 tipos principales de memoria:
 *	Stack/Pila: La memoria donde estan las variables locales, al salir del ambito dejan de estar reservadas
 * 	Heap/Monticulo: La memoria donde estan las variables que no se limitan a un ambito local cuya memoria es reservada de manera dinamica
 *
 * Existen 3 operadores principales para trabajar con punteros.
 *	&variable : Referenciar la variable obteniendo el puntero que lleva a ella
 * 	*variable : Dereferenciar la variable obteniendo el contenido al que apunta (solo debe ser utilizado con variables puntero, sino se producen comportamientos anomalos y fallos)
 *  tipo * : Significa variable del tipo "Puntero a tipo"
 *
 * Funciones a tener en cuenta para trabajar con punteros:
 *	free(puntero) : Libera la memoria reservada correspondiente al puntero.
 *			Solo debe usarse free con memoria reservada por el propio programador, las variables en la pila/stack se liberan solas al salir del ambito que las contiene.
 *			Intentar liberar punteros que no apunten a memoria en el heap (por ejemplo variables locales) lleva a error o comportamientos inesperados
 *	sizeof(tipo) : Devuelve el tamano en bytes necesario para almacenar una variable del tipo especificado
 *  malloc(tamano en bytes) : Reserva memoria del tamano recibido y devuelve un puntero al comienzo de esta
 *	calloc(numero de elementos, tipo de elemento) : Reserva memoria para un array del numero de elementos del tipo especificado.
 *			Los arrays estan ligados a los punteros en c pero todavia no se han comentado. Mas adelante se hablara de estos.
 *	realloc(puntero, tamano) : Reserva una zona de memoria del tamano especificado realojando la zona de memoria que empieza por el puntero
 * 			recibido. Devuelve el puntero al comienzo de la nueva zona de memoria.
 */
int main (void){
	int a = 5; // Variable a con un valor de 5
	// Imprimir el puntero que apunta a la variable a y tambien su valor
	printf("a: %p -> %d\n",&a,a); // %p indica al print que se espera recibir un puntero
	
	int * b = NULL; // La variable b es del tipo puntero a un int. Por defecto a punta a NULL.
	printf("b: %p\n",b);
	
	int * c = malloc(sizeof(int)); // Reserva memoria para un entero y la guarda en la variable c que es del tipo puntero a entero
	*c = 3; // Notese que c = valor asignaria valor a c que es una variable del tipo puntero. Para cambiar el valor de c hay que dereferenciarlo utilizando el operador *variable (*c en este caso)
	printf("%p -> c: %p -> %d\n",&c,c,*c); // &c obtiene la referencia a la variable c, que es un puntero. Por tanto &c devolvera un puntero a un puntero.
	free(c); // Libera la memoria que se habia reservado para la variable c
	
	int ** d = malloc(sizeof(int *)); // Reserva memoria para un puntero a un entero (d es un puntero a un puntero a un entero)
	*d = malloc(sizeof(int *)); // Reserva memoria para un entero
	**d = 1;
	printf("%p -> d: %p -> %p -> %d\n",&d,d,*d,**d); // En este caso &d es un puntero a un puntero a un puntero a un entero
	free(*d); // Primero se libera el puntero a un entero
	free(d); // Luego se libera el puntero al puntero. Si se hiciese al reves se perderia la referencia al puntero al entero y ya no se podria liberar, hay que tener cuidado y gestionar bien la memoria SIEMPRE.
	
	return EXIT_SUCCESS;
}