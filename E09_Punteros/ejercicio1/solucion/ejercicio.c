#include <stdlib.h>
#include <stdio.h>

/*
 * Alojar en el heap una variable entero b que tenga el valor de a.
 * Usar una variable c que sea un puntero a la variable a.
 * Tras esto modificar el valor de a pero no el de b.
 * Imprimir el valor de la variable a y los valores a los que apuntan los punteros b y c, observar lo que ocurre.
 */

int main (void){
	int a = 5;
	
	int * b = (int *) malloc(sizeof(int));
	*b = a;
	int * c = &a;
	a=7;
	
	printf("a = %d\n(*b) = %d\n(*c) = %d\n",a,*b,*c);
	
	free(b);
	return EXIT_SUCCESS;
}