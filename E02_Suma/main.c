#include <stdlib.h>
#include <stdio.h> 


int main (void){
	int a; // int a declara una variable de tipo entero
	a=3; // a=3 le asigna el valor 3 a la variable de tipo entero
	int b = 5; // int b = 5 declara una variable b de tipo entero y le asigna el valor 5, todo en la misma linea
	
	int c;
	c=a+b; // Asigna a la variable c la suma de los valores de las variables a y b
	
	printf("a=%d\n",a); // Imprime el valor de a. %d es un comodin que sera sustituido por un parametro entero. \n escapa un salto de linea.
	printf("b=%d\n",b);
	printf("c=%d\n",c);
	
	return EXIT_SUCCESS;
}
