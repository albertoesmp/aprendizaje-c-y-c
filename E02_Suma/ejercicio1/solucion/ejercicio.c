/*
 * Escribir un programa que sume dos variables en una tercera y, sin utilizar ninguna otra variable, actualice el valor
 * de la tercera por el suyo multiplicandose a si misma.
 * 
 * Imprimir el valor final de todas las variables.
 */

#include <stdlib.h>
#include <stdio.h>

int main (void){
	int a = 3;
	int b = 2;
	int c = a+b;
	c*=c; // c*=c equivale a c = c*c; Los operadores aritmeticos justo antes de un igual significan aplicar esa operacion tomando como operando el valor actual.
	
	printf("a=%d\nb=%d\nc=%d\n",a,b,c); // Los comodines para indicar parametros en la cadena de impresion siguen estrictamente el orden de aparicion de los parametros.

	return EXIT_SUCCESS;
}