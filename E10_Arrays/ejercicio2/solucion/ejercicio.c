#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/*
 * Calcular el modulo del vector U e imprimir el resultado.
 * Se puede usar la funcion sqrt para calcular la raiz cuadrada (si se usa gcc requiere compilar con -lm para linkar la libreria matematica).
 * A su vez tambien se dispone de la funcion pow(base,exponente) para el calculo de potencias
 */
 
int main (void){
	double u[3] = {1,7,4};
	
	double modulo = sqrt( pow(u[0],2) + pow(u[1],2) + pow(u[2],2) );

	printf("|u| = %f\n",modulo);
	
	return EXIT_SUCCESS;
}