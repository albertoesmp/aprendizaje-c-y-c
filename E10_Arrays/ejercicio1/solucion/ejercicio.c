#include <stdlib.h>
#include <stdio.h>

/*
 * Multiplicar las matrices a y b guardando el resultado en una nueva matriz c e imprimir el resultado
 */

int main (void){
	int a[3][3] = {	{1,3,5},
					{3,4,5},
					{2,4,6}};
					
	int b[3][3] = {	{2,4,8},
					{1,3,9},
					{5,7,3}};
	
	int c[3][3], i,j,x;
	for(i=0 ; i < 3 ; i++){ // Recorrer filas
		for(j=0 ; j < 3 ; j++){ // Recorrer columnas
			for(x=0 ; x < 3 ; x++){ // Recorrido en profundidad de la fila o columna correspondiente a la celda que se esta calculando
				if(x==0) c[i][j] = a[i][x] * b[x][j]; // La primera vez se inicializa (no seria necesario si ya se hubiese inicializado, pero al no inicializar el valor podria ser cualquiera)
				else c[i][j] += a[i][x] * b[x][j];
			}
		}
	}
	
	for(i = 0 ; i < 3 ; i++){ // Recorrer filas
		for(j = 0 ; j < 3 ; j++){ // Recorrer columnas
			printf("%-2d ",c[i][j]);
		}
		printf("\n");
	}
	
	return EXIT_SUCCESS;
}