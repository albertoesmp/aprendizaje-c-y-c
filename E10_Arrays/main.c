#include <stdlib.h>
#include <stdio.h>

/*
 * Los arrays son zonas de almacenamiento contiguo en memoria que contienen elementos del mismo tipo.
 * Pueden utilizarse para representar vectores, matrices, tablas, estructuras multidimensionales, etc.
 *
 * Los arrays pueden gestionarse de manera local utilizando el operador [].
 *		Ej: int enteros[5]; Reserva un array de 5 enteros
 * Para asignar valores a los elementos del array se hacen accediendo a la posicion (tener en cuenta que el primer elemento es el 0 y no el 1,
 *		por tanto un array de 5 elementos tendra posiciones desde la 0 hasta la 4).
 * 
 * Por ejemplo para asignar o acceder a los valores del array anterior:
 *		enteros[0] Obtiene el valor del elemento en la posicion 0 (la primera) del array
 *		enteros[1] = 1337; Fija el valor de la posicion 1 (la segunda) del array a 1337
 *
 * Existe una manera de asignar un conjunto de valores al array en el momento de su declaracion (Y SOLO EN ESE MOMENTO)
 *		Ej: int enteros[5] = {1,2,3,4,5}; Inicializa el array de 5 enteros con los numeros 1, 2, 3, 4 y 5
 *
 * Los arrays tambien pueden reservarse utilizando funciones de gestion de memoria, de esta manera trascienden del ambito local de su declaracion.
 *		Ej: int * enteros = (int *) malloc(sizeof(int)*5); Reserva memoria para un array de 5 enteros, al cual se accede igual que para un array local.
 *		Ej de acceso: enteros[0]
 *
 * Tambien se pueden recorrer los arrays con aritmetica de punteros:
 *		La expresion enteros[1] equivale a dereferenciar (enteros+1), es decir, equivale a *(enteros+1)
 */
int main (void){
	int i;
	
	// Array local de enteros (ubicado en el stack/pila)
	int enteros[5] = {1,2,3,4,5};
	for(i=0 ; i < 5 ; i++) printf("enteros[%d] = %d\n",i,enteros[i]);
	printf("\n");
	
	// Modificar array local de enteros para que tenga los 5 primeros multiplos de 10
	for(i=0 ; i < 5 ; i++) enteros[i] = 5*i;
	for(i=0 ; i < 5 ; i++) printf("enteros[%d] = %d\n",i,enteros[i]);
	printf("\n");
	
	// Crear un array en el heap
	double * decimales = (double *) malloc(sizeof(double)*5);
	for(i=0 ; i < 5 ; i++) *(decimales+i) = i*0.369;
	for(i=0 ; i < 5 ; i++) printf("decimales[%d] = %f\n",i,decimales[i]);
	printf("\n");
	
	// Modifica el array de decimales para que tenga los 5 pimeros multiplos de 13.37
	for(i=0 ; i < 5 ; i++) decimales[i] = i*13.37;
	for(i=0 ; i < 5 ; i++) printf("decimales[%d] = %f\n",i,decimales[i]);
	printf("\n");
	
	return EXIT_SUCCESS;
}