/*
 * Escribir un programa que puede usar como maximo 2 variables.
 * El programa tomara en consideracion 2 numeros.
 * Cuando al primer numero se le reste el segundo y aun asi siga siendo mas grande que este
 * imprimir un mensaje diciendo que el primer numero es mucho mas grande que el segundo.
 * Si no se cumple esta condicion imprimir un mensaje diciendo que el primer numero es un poco mas grande que el segundo.
 */
#include <stdlib.h>
#include <stdio.h>

int main(void){
	int a = 20;
	int b = 9;
	
	if((a-b)>b) printf("%d es MUCHO mas grande que %d\n",a,b);
	else printf("%d es un poco mas grande que %d\n",a,b);
	
	return EXIT_SUCCESS;
}