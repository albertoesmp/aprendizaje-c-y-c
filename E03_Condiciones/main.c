#include <stdlib.h>
#include <stdio.h>

int main (void){
	int a = 5;
	int b = 4;
	
	if(a>b){ // Si se cumple que a es mayor que b ejecuta las instrucciones entre llaves {}
		printf("a es mayor que b\n");
	}
	else{ // Si no se cumplio el if inmediatamente anterior ejecutara estas otras instrucciones
		printf("a NO es mayor que b\n");
	}
	
	if(b>a){
		printf("b es mayor que a\n");
	}
	else{
		printf("b NO es mayor que a\n");
	}
	
	if(a==b){
		printf("a es igual a b\n");
	}
	else{
		printf("a NO es igual a b\n");
	}
	
}