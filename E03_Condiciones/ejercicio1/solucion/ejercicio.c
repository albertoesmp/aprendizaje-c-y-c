/*
 * Escribir un programa que tenga dos variables numericas e imprima por pantalla el nombre y el valor
 * de la variable que corresponda al numero mas grande.
 * Si los numeros son iguales debe imprimir un mensaje diciendo que ninguno es mas grande
 */

#include <stdlib.h>
#include <stdio.h>

int main (void){
		int a = 12;
		int b = 11;
		
		if(!(a==b)){ // El operador ! significa negacion. Si a==b es cierto al ir precedido de un ! sera falso, si fuese falso al ir precedido por ! seria cierto
			// "!(a==b)" equivale logicamente a "a!=b"
			if(a>b) // Cuando solo hay una instruccion despues del if no es necesario incluirla entre llaves {}
				printf("A = %d\n",a);
			else
				printf ("B = %d\n",b);
		}
		else printf("Ninguno de los numeros era mas grande que el otro.");

	return EXIT_SUCCESS;
}