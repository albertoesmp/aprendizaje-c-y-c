#include <stdlib.h>
#include <stdio.h>

/*
 * Las macros en c son fragmentos de codigo a los que se les da un nombre en tiempo de compilacion.
 * Siempre que se use dicho nombre sera sustituide por el fragmento de codigo al que representa.
 */

// Uso de macros y definiciones de tipos para construir el tipo boolean
typedef int boolean;
#define TRUE 1 // Siempre que se vea TRUE se reemplazara por 1
#define FALSE 0 // Siempre que se vea FALSE se reemplazara por 0
 
int main (void){
	// Programa que usa el tipo creado para comparar 2 numeros introducidos por el usuario
	boolean b;
	int x, y;
	
	printf("Introduce valor para x: ");
	scanf("%d",&x);
	printf("Introduce valor para y: ");
	scanf("%d",&y);
	if(x>y) b = TRUE; else b = FALSE;
	
	if(b) printf("x(%d) es mayor que y(%d)\n",x,y);
	else printf("x(%d) NO es mayor que y(%d)\n",x,y);

	return EXIT_SUCCESS;
}