#include "cola.h"

// *** TIPOS DE DATOS *** //
// ********************** //
struct s_cola{ // Estructura utilizada para almacenar los elementos de la cola
	Elemento * elementos; // Elementos contiguos en memoria gestionada dinamicamente
	int numero_elementos; // Numero de elementos encolados
	
}; 

// *** FUNCIONES *** //
// ***************** //
void crear (Cola * cola){
	*cola = (struct s_cola *) malloc(sizeof(struct s_cola));
	struct s_cola * c = (struct s_cola *) *cola;
	
	c->numero_elementos = 0;
	c->elementos = NULL;
}

void destruir(Cola * cola){
	struct s_cola * c = (struct s_cola *) *cola;
	if(c->elementos!=NULL) free(c->elementos);
	free(c);
}

void insertar (Cola * cola, Elemento elemento){
	struct s_cola * c = (struct s_cola *) *cola;

	// Gestionar la memoria dinamica para dejar sitio para la nueva insercion
	if(c->elementos==NULL)
		c->elementos = (Elemento *) malloc(sizeof(Elemento)*c->numero_elementos+1);
	else
		c->elementos = (Elemento *) realloc(c->elementos, sizeof(Elemento)*c->numero_elementos+1);
	
	// Insertar elemento
	c->elementos[c->numero_elementos] = elemento;
	c->numero_elementos++;
}

int tiene_siguiente(Cola cola){
	struct s_cola * c = (struct s_cola *) cola;
	return c->numero_elementos>0;
}

Elemento siguiente (Cola * cola){
	// Obtener elemento
	struct s_cola * c = (struct s_cola *) *cola;
	Elemento e = c->elementos[0];
	
	// Reordenar elementos
	int i;
	for(i=0 ; i < c->numero_elementos-1 ; i++){
		c->elementos[i] = c->elementos[i+1];
	}
	
	// Ajustar memoria reservada dinamicamente
	c->numero_elementos--;
	if(c->numero_elementos==0){
		free(c->elementos);
		c->elementos = NULL;
	}
	else{
		c->elementos = (Elemento *) realloc(c->elementos, c->numero_elementos);
	}
	
	return e;
}