#include <stdlib.h>

/*
 * Cola de almacenamiento de tamano fijo y contiguo para sus elementos
 */

#define TAM_COLA 32 // Tamano de la cola, numero de elementos que puede almacenar

/*
 * TAD de una cola que soporta hasta 32 elementos
 */

// *** TIPOS DE DATOS *** //
// ********************** //
typedef void * Cola; // Tipo de datos usado para representar la Cola
typedef int Elemento; // Tipo de datos que define lo que es un elemento de la cola, puede sustituirse a voluntad incluso por un puntero (void *) para generalizar el dato

// *** FUNCIONES *** //
// ***************** //
void crear (Cola * cola); // Crea una cola, reservando los recursos asociados a la misma
void destruir(Cola * cola); // Destruye una cola, liberando los recursos asociados a la misma
void insertar (Cola * cola, Elemento elemento); // Inserta un elemento en una cola
int tiene_siguiente(Cola cola); // Devuelve 1 si la cola tiene un elemento siguiente, 0 si no lo tiene
Elemento siguiente (Cola * cola); // Extrae el siguiente elemente de la cola

