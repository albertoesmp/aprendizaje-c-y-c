#include "cola.h"

// *** TIPOS DE DATOS *** //
// ********************** //
struct s_cola{ // Estructura utilizada para almacenar los elementos de la cola
	Elemento array[TAM_COLA]; // Array utilizado para almacenar los elementos
	int posicion_ultimo_elemento; // Posicion del ultimo elemento insertado
	/*
	 * orden es un vector que lleva cuenta del orden en que se realizan las inserciones.
	 * El primer valor distinto de -1 que se encuentre en orden corresponde al elemento siguiente
	 * en salir de la cola
	 */
	int orden[TAM_COLA];
}; 

// *** FUNCIONES *** //
// ***************** //
void crear (Cola * cola){
	*cola = (struct s_cola *) malloc(sizeof(struct s_cola));
	struct s_cola * c = (struct s_cola *) *cola;
	c->posicion_ultimo_elemento = -1;
	
	int i;
	for(i=0 ; i < 32 ; i++){
		c->orden[i] = -1;
	}
}

void destruir(Cola * cola){
	free(*cola);
}

void insertar (Cola * cola, Elemento elemento){
	// Realizar la insercion en la cola
	struct s_cola * c = (struct s_cola *) *cola;
	c->posicion_ultimo_elemento++;
	c->array[c->posicion_ultimo_elemento] = elemento;
	
	// Indicar el orden correspondiente
	int i;
	for(i=0 ; i < TAM_COLA ; i++){
		if(c->orden[i] == -1){
			c->orden[i] = c->posicion_ultimo_elemento;
			break;
		}
	}
}

int tiene_siguiente(Cola cola){
	struct s_cola * c = cola;
	int i;
	for(i=0 ; i < TAM_COLA ; i++){
		if(c->orden[i]!=-1) return 1;
	}
	return 0;
}

Elemento siguiente (Cola * cola){
	struct s_cola * c = (struct s_cola *) *cola;
	
	// Obtener siguiente elemento
	int i, indice_siguiente;
	for(i=0, indice_siguiente=-1; i < TAM_COLA ; i++){
		if(c->orden[i]!=-1){
			indice_siguiente = c->orden[i];
			c->orden[i] = -1;
			break;
		}
	}

	Elemento e = c->array[indice_siguiente];
	
	// Reorganizar el array de elementos
	for(i=indice_siguiente ; i < c->posicion_ultimo_elemento; i++){
		c->array[i] = c->array[i+1]; // i+1 pues la posicion del ultimo elemento ha sido disminuida y como maximo sera TAM_COLA-2 si se usa bien la cola
	}
	
	// Modificar variable que indica la posicion del ultimo elemento
	c->posicion_ultimo_elemento--;
	
	// Reorganizar el array de orden
	for(i=0 ; i < TAM_COLA-1 ; i++){
		c->orden[i] = c->orden[i+1];
		if(c->orden[i]>indice_siguiente) c->orden[i]--;
	}
	c->orden[TAM_COLA-1] = -1;
	
	return e;
}