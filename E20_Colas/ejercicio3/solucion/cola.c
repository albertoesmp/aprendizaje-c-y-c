#include "cola.h"

// *** TIPOS DE DATOS *** //
// ********************** //
typedef struct s_elemento_cola{
	Elemento elemento; // Este elemento
	struct s_elemento_cola * siguiente_elemento; // Elemento siguiente a este en la cola
} ElementoCola;

struct s_cola{ // Estructura utilizada para almacenar los elementos de la cola
	ElementoCola * elemento;
	
}; 

// *** FUNCIONES *** //
// ***************** //
void crear (Cola * cola){
	*cola = (struct s_cola *) malloc(sizeof(struct s_cola));
	struct s_cola * c = (struct s_cola *) *cola;
	
	c->elemento = NULL;
}

void destruir(Cola * cola){
	struct s_cola * c = (struct s_cola *) *cola;
	
	// Vaciar cola
	if(c->elemento!=NULL){
		ElementoCola * ec = NULL;
		do{
			ec = c->elemento;
			if(ec->siguiente_elemento!=NULL) c->elemento=ec->siguiente_elemento;
			free(ec);
		
		}while(c->elemento!=NULL);
	}
	
	// Liberar cola
	free(c);
}

void insertar (Cola * cola, Elemento elemento){
	struct s_cola * c = (struct s_cola *) *cola;
	
	ElementoCola * ec = (ElementoCola *) malloc(sizeof(ElementoCola));
	ec->elemento = elemento;
	ec->siguiente_elemento = NULL;
	
	// Obtener ultimo elemento de la cola a insertar a continuacion
	ElementoCola * aux = c->elemento;
	ElementoCola * ultimo = NULL;
	while( aux != NULL){
		ultimo = aux;
		aux = ultimo->siguiente_elemento;
	}
	if(ultimo==NULL) c->elemento = ec;
	else ultimo->siguiente_elemento = ec;
}

int tiene_siguiente(Cola cola){
	struct s_cola * c = (struct s_cola *) cola;
	return c->elemento!=NULL;

}

Elemento siguiente (Cola * cola){
	// Obtener elemento
	struct s_cola * c = (struct s_cola *) *cola;
	ElementoCola * siguiente = c->elemento;
	c->elemento = siguiente->siguiente_elemento;
	Elemento e = siguiente->elemento;
	free(siguiente);
	return e;
}