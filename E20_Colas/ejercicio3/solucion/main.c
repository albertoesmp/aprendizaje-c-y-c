#include <stdlib.h>
#include <stdio.h>
#include "cola.h"

int main (void){
	Cola cola;
	crear(&cola);
	
	insertar(&cola,1);
	insertar(&cola,2);
	insertar(&cola,3);
	insertar(&cola,4);
	insertar(&cola,5);
	
	Elemento e = siguiente(&cola);
	printf("Elemento extraido de la cola: %d\n",e);
	e = siguiente(&cola);
	printf("Elemento extraido de la cola: %d\n",e);
	e = siguiente(&cola);
	printf("Elemento extraido de la cola: %d\n",e);
	
	insertar(&cola,6);
	insertar(&cola,7);
	insertar(&cola,8);
	insertar(&cola,9);
	
	
	while(tiene_siguiente(cola)){
		Elemento e = siguiente(&cola);
		printf("Elemento extraido de la cola: %d\n",e);
	}
	
	destruir(&cola);

	return EXIT_SUCCESS;
}